#
# Perl %name: CMEnvironment.pl % (%full_filespec: CMEnvironment.pl~13:perl:emd#1 %)
# %derived_by: wbirkhil %
# %date_created: Wed Nov  2 08:38:20 2011 %
#
use strict;

my %_CHANGE_PORT_DB_PATH;
$_CHANGE_PORT_DB_PATH{'emdlagvmas1109'}->{'8600'} = "/opt/ccmdb/test";
$_CHANGE_PORT_DB_PATH{'emdlagvmas1109'}->{'8601'} = "/opt/ccmdb/sandbox_z";
$_CHANGE_PORT_DB_PATH{'emdlagvmas1109'}->{'8602'} = "/opt/ccmdb/sandbox_y";
$_CHANGE_PORT_DB_PATH{'emdlagvmas1109'}->{'8603'} = "/opt/ccmdb/sandbox_x";
$_CHANGE_PORT_DB_PATH{'emdlagvmas1109'}->{'8604'} = "/opt/ccmdb/sandbox_w";

my %_NO_PRIVS_USERNAME;
$_NO_PRIVS_USERNAME{'emdlagvmas1109'}->{'8600'} = "espluser";
$_NO_PRIVS_USERNAME{'emdlagvmas1109'}->{'8601'} = "espluser";
$_NO_PRIVS_USERNAME{'emdlagvmas1109'}->{'8602'} = "espluser";
$_NO_PRIVS_USERNAME{'emdlagvmas1109'}->{'8603'} = "espluser";
$_NO_PRIVS_USERNAME{'emdlagvmas1109'}->{'8604'} = "espluser";

my %_NO_PRIVS_PASS;
$_NO_PRIVS_PASS{'emdlagvmas1109'}->{'8600'} = "bubba12";
$_NO_PRIVS_PASS{'emdlagvmas1109'}->{'8601'} = "bubba12";
$_NO_PRIVS_PASS{'emdlagvmas1109'}->{'8602'} = "bubba12";
$_NO_PRIVS_PASS{'emdlagvmas1109'}->{'8603'} = "bubba12";
$_NO_PRIVS_PASS{'emdlagvmas1109'}->{'8604'} = "bubba12";

my %_PATH_TO_CHANGE_INSTALL;
$_PATH_TO_CHANGE_INSTALL{'emdlagvmas1109'}->{'8600'} = "/opt/IBM/Rational/Change/52_8600";
$_PATH_TO_CHANGE_INSTALL{'emdlagvmas1109'}->{'8601'} = "/opt/IBM/Rational/Change/52_8601z";
$_PATH_TO_CHANGE_INSTALL{'emdlagvmas1109'}->{'8602'} = "/opt/IBM/Rational/Change/52_8602y";
$_PATH_TO_CHANGE_INSTALL{'emdlagvmas1109'}->{'8603'} = "/opt/IBM/Rational/Change/52_8603x";
$_PATH_TO_CHANGE_INSTALL{'emdlagvmas1109'}->{'8604'} = "/opt/IBM/Rational/Change/52_8604w";

my $_RELATIVE_PATH_TO_ETC_DIR = "/jetty/webapps/change/WEB-INF/wsconfig/templates/pt/etc";

sub get_relative_etc_path{
	return $_RELATIVE_PATH_TO_ETC_DIR;
}

sub get_no_privs_user{
	# 
	# returns the no_privs user for the given server and port to be used by web_reports
	#
	
	my $servername = $_[0];
	my $change_port = $_[1];

	if($_NO_PRIVS_USERNAME{$servername}->{$change_port}){
		return $_NO_PRIVS_USERNAME{$servername}->{$change_port};
	}
	else{
		return "";
	}
}

sub get_no_privs_pass{
	# 
	# returns the no_privs pass for the given server and port to be used by web_reports
	#

	my $servername = $_[0];
	my $change_port = $_[1];

	if($_NO_PRIVS_PASS{$servername}->{$change_port}){
		return $_NO_PRIVS_PASS{$servername}->{$change_port};
	}
	else{
		return "";
	}
}

sub getDBpathForChangePort{
	#
	# Get the optbase path given server and change port
	#

	my $servername = $_[0];
	my $change_port = $_[1];

	if($_CHANGE_PORT_DB_PATH{$servername}->{$change_port}){
		return $_CHANGE_PORT_DB_PATH{$servername}->{$change_port};
	}
	else{
		return "";
	}
}

sub getCCMHOME
{
	# Get the appropriate path for the CCM_HOME environment variable, based on the optbase path.
	#
	#  Parameters:
	#	db -	optbase path
	
	my $db = $_[0];

	if(!(useXInstallation($db))){ return("/opt/IBM/Rational/Synergy/7.1"); }
	else { return("opt/IBM/Rational/Synergy/7.1"); }
	

}

sub setEnvVarsForCMSession
{
	# Sets up environment to connect to Synergy from a CR trigger
	#
	#  Parameters:
	#	db -	optbase path

	my $db = $_[0];
	my $CCM_HOME_path = getCCMHOME($db);
	my $useX = useXInstallation($db);
	my $logFileEnd = "\.log";
	if ($useX) { $logFileEnd = "x" . $logFileEnd; }
	my $uilog = $ENV{HOME} . "/ccm_ui" . $logFileEnd;
	my $englog = $ENV{HOME} . "/ccm_eng" . $logFileEnd;
	
	$ENV{CCM_HOME} = $CCM_HOME_path;
	$ENV{PATH} = $ENV{PATH} . ":$CCM_HOME_path/bin:/usr/bin:";
	$ENV{CCM_UILOG} = $uilog;
	$ENV{CCM_ENGLOG} = $englog;
}

sub getTCInstallPath
{
	# Get the appropriate path for the Change installation, based on the optbase path.
	#
	#  Parameters:
	#	db -	optbase path
		
	my $db = $_[0];

	#return("/opt/IBM/Rational/Change/52_8604w");



	if ($db =~ /^\/opt\/ccmdb\/sandbox_z/) { return($_PATH_TO_CHANGE_INSTALL{'emdlagvmas1109'}->{'8601'}); }
	if ($db =~ /^\/opt\/ccmdb\/sandbox_y/) { return($_PATH_TO_CHANGE_INSTALL{'emdlagvmas1109'}->{'8602'}); }
	if ($db =~ /^\/opt\/ccmdb\/sandbox_x/) { return($_PATH_TO_CHANGE_INSTALL{'emdlagvmas1109'}->{'8603'}); }
	if ($db =~ /^\/opt\/ccmdb\/sandbox_w/) { return($_PATH_TO_CHANGE_INSTALL{'emdlagvmas1109'}->{'8604'}); }
	if ($db =~ /^\/opt\/ccmdb\/test/) { return($_PATH_TO_CHANGE_INSTALL{'emdlagvmas1109'}->{'8600'}); }
	return("");
}

sub useXInstallation
{
	# Determines whether to use the non-X or X CM installation, based on the optbase path.
	#
	#  Returns:
	#	0 -	if optbase is connected to the non-X installation
	#	1 -	if optbase is connected to the X installation
	#
	#  Parameters:
	#	db -	optbase path
	
	my $db = $_[0];

	if ($db =~ /^\/opt\/ccmdb\//) { return(0); }
	elsif ($db =~ /^\/opt\/ccmdb\//) { return(0); }
	elsif ($db =~ /^\/opt\/ccmdb\/sandbox/) { return(1); }
	else { return(1); }
}

1;
