#	%full_filespec: CRDEFS.pl~45:perl:emd#1 %
#	%date_created:  Wed Jan  4 14:23:05 2012 %		%created_by: lawrence %

my %_cr_copyrelationships;
$_cr_copyrelationships{""} = "START_HERE";
$_cr_copyrelationships{"Release"} = "START_HERE";
$_cr_copyrelationships{"Change Control"} = "related_product_cr";
$_cr_copyrelationships{"Database Change"} = "db_child";
$_cr_copyrelationships{"Spec Development"} = "spec_child";
$_cr_copyrelationships{"Software Development"} = "sw_child";
$_cr_copyrelationships{"Traceability Development"} = "trace_child";

my %_childcr_prependtext;
$_childcr_prependtext{"Database Change"} = "DB:";
$_childcr_prependtext{"Spec Development"} = "SPEC:";
$_childcr_prependtext{"Software Development"} = "SW:";
$_childcr_prependtext{"Traceability Development"} = "TRACE";

my %_lifecycletag;
$_lifecycletag{"Database Change"} = "db";
$_lifecycletag{"Spec Development"} = "spec";
$_lifecycletag{"Software Development"} = "sw";
$_lifecycletag{"Release"} = "rls";
$_lifecycletag{"Traceability Development"} = "trace";

# Standard values used in due date calculations on Release CRs

my %_efficiency_rlsbld;
$_efficiency_rlsbld{""} = 1;
$_efficiency_rlsbld{"EM2000"} = 1;
$_efficiency_rlsbld{"FIRE"} = 1;
$_efficiency_rlsbld{"IntelliTrain"} = 1;
$_efficiency_rlsbld{"MPU"} = 1;
$_efficiency_rlsbld{"CSDB"} = 0.65;
$_efficiency_rlsbld{"QNX"} = 0.7;

my %_efficiency_swdev;
$_efficiency_swdev{""} = 1;
$_efficiency_swdev{"EM2000"} = 0.25;
$_efficiency_swdev{"FIRE"} = 0.25;
$_efficiency_swdev{"IntelliTrain"} = 0.35;
$_efficiency_swdev{"MPU"} = 1;
$_efficiency_swdev{"CSDB"} = 0.35;
$_efficiency_swdev{"QNX"} = 0.21;

my $_efficiency_specdev = .5;
my $_efficiency_tracdev = 0.65;

my %_dwelltime_spec2sw;			# Dwell time, in working days, between spec due date and sw start date
$_dwelltime_spec2sw{""} = 0;
$_dwelltime_spec2sw{"EM2000"} = 0;
$_dwelltime_spec2sw{"FIRE"} = 0;
$_dwelltime_spec2sw{"IntelliTrain"} = 0;
$_dwelltime_spec2sw{"MPU"} = 0;
$_dwelltime_spec2sw{"CSDB"} = 0;

# End of standard values used in due date calculations on Release CRs


# Standard values used in due date calculations on SW Deliverable CRs
my $_pkg_duration_char = 10;
my $_pkg_duration_can = 3;
my $_pkg_duration_ft_rel = 15;
my $_pkg_duration_ft_rel_ext = 20;

my %_pkg_duration_subsys;	
$_pkg_duration_subsys{"EM2000"} = 10;
$_pkg_duration_subsys{"PowerPlant"} = $_pkg_duration_subsys{"EM2000"};	# Power Plant and EM2000 use the same duration
$_pkg_duration_subsys{"FIRE"} = 10;
$_pkg_duration_subsys{"LCC_CAN"} = 5;
$_pkg_duration_subsys{"MPU"} = 5;
$_pkg_duration_subsys{"Install"} = 5;
$_pkg_duration_subsys{"AirBrake"} = 5;
$_pkg_duration_subsys{"EMDEC"} = 5;

my %_pkg_duration_subsys_ext;
$_pkg_duration_subsys_ext{"EM2000"} = 15;
$_pkg_duration_subsys_ext{"PowerPlant"} = $_pkg_duration_subsys_ext{"EM2000"};	# Power Plant and EM2000 use the same duration
$_pkg_duration_subsys_ext{"FIRE"} = 15;
$_pkg_duration_subsys_ext{"LCC_CAN"} = 10;
$_pkg_duration_subsys_ext{"MPU"} = 10;
$_pkg_duration_subsys_ext{"Install"} = 10;
$_pkg_duration_subsys_ext{"AirBrake"} = 10;
$_pkg_duration_subsys_ext{"EMDEC"} = 10;

# End of standard values used in due date calculations on SW Deliverable CRs

my %_product_name;			# Product name for each component_name (component_name as used in release tags)
# Product name for each component_name. This is used to
# 	determine the timeline (i.e. which standard values are
#	used in due date calculations) for the target release.
#	Each separate entry in the Product listbox can either use 
#	the default release timeline or define it's own. 
#	(There is no Product attribute on the Release CR, so the
#	settings are determined based on the Target Release.)
#
$_product_name{""} = "";
$_product_name{"CSDB"}						= "CSDB";
$_product_name{"CSLib"} 					= "Control System Library";
$_product_name{"EM2K"} 						= "EM2000";
$_product_name{"FastBrake"} 				= "FIRE";
$_product_name{"FIRE"} 						= "FIRE";
$_product_name{"F-DT"} 						= "FIRE";
$_product_name{"F-Install"} 				= "FIRE";
$_product_name{"F-IntelliTrain"}			= "IntelliTrain";
$_product_name{"F-OS"} 						= "FIRE";
$_product_name{"ILAB"} 						= "ILAB";
$_product_name{"LCC_CAN"} 					= "EM2000";
$_product_name{"MPU"} 						= "MPU";
$_product_name{"PowerPlant"} 				= "EM2000";
$_product_name{"Process_Asset_Library"} 	= "";

my %_release_stream_level;
#	0 for components whose Target Release = Synergy Release Tag 
#	1 if component uses release streams and the release_target_id is equivalent
#		to the patch release (ex. 00, 01, 02, etc.)
#	2 if component uses release streams and the release_target_id is equivalent
#		to the minor.patch
#
$_release_stream_level{""} 							= 0;
$_release_stream_level{"CSDB"}						= 1;
$_release_stream_level{"CSLib"}						= 1;
$_release_stream_level{"EM2K"} 						= 1;
$_release_stream_level{"FastBrake"} 				= 1;
$_release_stream_level{"FIRE"} 						= 1;
$_release_stream_level{"F-Install"} 				= 1;
$_release_stream_level{"F-IntelliTrain"} 			= 1;
$_release_stream_level{"ILAB"} 						= 1;
$_release_stream_level{"LCC_CAN"} 					= 1;
$_release_stream_level{"MPU"} 						= 1;
$_release_stream_level{"PowerPlant"} 				= 1;
$_release_stream_level{"Process_Asset_Library"} 	= 1;

my %_uses_release_crs;
#	0 if component_name is for a product/system that does not use Release CRs
#	1 if component_name is for a product/system that uses Release CRs
#
#	Note: All products that use release streams (release stream level > 0)
#		must use Release CRs in order to populate the Target Release
#		listbox.
$_uses_release_crs{""}			 				= 0;
$_uses_release_crs{"CSDB"}						= 1;
$_uses_release_crs{"CSLib"}						= 1;
$_uses_release_crs{"EM2K"}			 			= 1;
$_uses_release_crs{"EMDEC"}                                             = 1;
$_uses_release_crs{"FastBrake"} 				= 1;
$_uses_release_crs{"FIRE"} 						= 1;
$_uses_release_crs{"F-Install"} 				= 1;
$_uses_release_crs{"F-IntelliTrain"} 			= 1;
$_uses_release_crs{"ILAB"}			 			= 1;
$_uses_release_crs{"LCC_CAN"}					= 1;
$_uses_release_crs{"MPU"}			 			= 1;
$_uses_release_crs{"PowerPlant"}		 		= 1;
$_uses_release_crs{"Process_Asset_Library"} 	= 1;

my %_process_name;
# Converts release component name to process name used in release creation
$_process_name{""} 							= "Core Development Process";
$_process_name{"CSDB"}						= "Core Development Process";
$_process_name{"CSLib"} 							= "Core Development Process";
$_process_name{"EM2K"} 						= "Core Development Process";
$_process_name{"FastBrake"} 				= "Core Development Process";
$_process_name{"FIRE"} 						= "Core Development Process";
$_process_name{"F-DT"} 						= "Core Development Process";
$_process_name{"F-Install"} 				= "Core Development Process";
$_process_name{"F-IntelliTrain"}			= "Core Development Process";
$_process_name{"F-OS"} 						= "Core Development Process";
$_process_name{"ILAB"} 						= "Core Development Process";
$_process_name{"LCC_CAN"} 					= "Core Development Process";
$_process_name{"MPU"} 						= "Core Development Process";
$_process_name{"PowerPlant"}	 			= "Core Development Process";
$_process_name{"Process_Asset_Library"}		= "Core Development Process";

my %_product_to_release_tgt_query_str;
# Each product's associated target release query string
# Currently used to filter Release CRs by product for all products that are subsystems of a package
$_product_to_release_tgt_query_str{'EM2000'} = "release_target match 'E??.*' or release_target match 'EM2K/*'";
$_product_to_release_tgt_query_str{'FIRE'} = "release_target match 'F??.*' or release_target match 'FIRE/*'";
$_product_to_release_tgt_query_str{'LCC_CAN'} = "release_target match 'LCC_CAN/*'";
$_product_to_release_tgt_query_str{'MPU'} = "release_target match 'M??.*' or release_target match 'MPU/*'";
$_product_to_release_tgt_query_str{'AirBrake'} = "release_target match '*Brake*'";
$_product_to_release_tgt_query_str{'Install'} = "release_target match 'F-Install*'";
$_product_to_release_tgt_query_str{'PowerPlant'} = "release_target match 'P??.*' or release_target match 'PowerPlant/*'";
$_product_to_release_tgt_query_str{'EMDEC'} = "release_target match '*EMDEC/*'";

my %_companion_os_name;
# Maps Operating System name to the "Companion" operating system.  This is used for finding and creating OS Companion CRs for FIRE.
$_companion_os_name{"QNX"} 		= "XP";
$_companion_os_name{"XP"}	 	= "QNX";

# The Change GUI uses the .js parallel to the following variables, which have the same variable
#	names, and are defined in the shared_tables.js file in the package template.  Any changes to
#	the logic and/or values here need to be reflected in the shared_tables.js file as well.
#


# Mapping of subsystem info attributes on Pkg Subsystem CRs to attributes on SW Deliverable CRs
# 	Format:  $_pkgsubsys_swdlv_subsys_attr_map{"PKG_SUBSYSTEM_NAME"}->{"PKGSUBSYS_ATTR_NAME"} = "SWDLV_ATTR_NAME";
#	Where:
#		PKG_SUBSYSTEM_NAME = 
#		PKGSUBSYS_ATTR_NAME = Name of the attribute on the Pkg Subsystem CR
#		SWDLV_ATTR_NAME = Name of the attribute on the SW Deliverable CR
my %_pkgsubsys_swdlv_subsys_attr_map;
$_pkgsubsys_swdlv_subsys_attr_map{"AirBrake"}->{"request_type"} = "request_cat_airbrake";
$_pkgsubsys_swdlv_subsys_attr_map{"AirBrake"}->{"release_target"} = "release_tgt_airbrake";
$_pkgsubsys_swdlv_subsys_attr_map{"AirBrake"}->{"config_id"} = "config_id_airbrake";
$_pkgsubsys_swdlv_subsys_attr_map{"AirBrake"}->{"resolver"} = "resolver_airbrake";
$_pkgsubsys_swdlv_subsys_attr_map{"AirBrake"}->{"problem_description"} = "subsys_changes_airbrake";
$_pkgsubsys_swdlv_subsys_attr_map{"EM2000"}->{"request_type"} = "request_cat_em2000";
$_pkgsubsys_swdlv_subsys_attr_map{"EM2000"}->{"release_target"} = "release_tgt_em2000";
$_pkgsubsys_swdlv_subsys_attr_map{"EM2000"}->{"config_id"} = "config_id_em2000";
$_pkgsubsys_swdlv_subsys_attr_map{"EM2000"}->{"resolver"} = "resolver_em2000";
$_pkgsubsys_swdlv_subsys_attr_map{"EM2000"}->{"problem_description"} = "subsys_changes_em2000";
$_pkgsubsys_swdlv_subsys_attr_map{"FIRE"}->{"request_type"} = "request_cat_fire";
$_pkgsubsys_swdlv_subsys_attr_map{"FIRE"}->{"release_target"} = "release_tgt_fire";
$_pkgsubsys_swdlv_subsys_attr_map{"FIRE"}->{"config_id"} = "config_id_fire";
$_pkgsubsys_swdlv_subsys_attr_map{"FIRE"}->{"resolver"} = "resolver_fire";
$_pkgsubsys_swdlv_subsys_attr_map{"FIRE"}->{"problem_description"} = "subsys_changes_fire";
$_pkgsubsys_swdlv_subsys_attr_map{"LCC_CAN"}->{"request_type"} = "request_cat_lcccan";
$_pkgsubsys_swdlv_subsys_attr_map{"LCC_CAN"}->{"release_target"} = "release_tgt_lcccan";
$_pkgsubsys_swdlv_subsys_attr_map{"LCC_CAN"}->{"config_id"} = "config_id_lcccan";
$_pkgsubsys_swdlv_subsys_attr_map{"LCC_CAN"}->{"resolver"} = "resolver_lcccan";
$_pkgsubsys_swdlv_subsys_attr_map{"LCC_CAN"}->{"problem_description"} = "subsys_changes_lcccan";
$_pkgsubsys_swdlv_subsys_attr_map{"MPU"}->{"request_type"} = "request_cat_mpu";
$_pkgsubsys_swdlv_subsys_attr_map{"MPU"}->{"release_target"} = "release_tgt_mpu";
$_pkgsubsys_swdlv_subsys_attr_map{"MPU"}->{"config_id"} = "config_id_mpu";
$_pkgsubsys_swdlv_subsys_attr_map{"MPU"}->{"resolver"} = "resolver_mpu";
$_pkgsubsys_swdlv_subsys_attr_map{"MPU"}->{"problem_description"} = "subsys_changes_mpu";
$_pkgsubsys_swdlv_subsys_attr_map{"PowerPlant"}->{"request_type"} = "request_cat_powerplant";
$_pkgsubsys_swdlv_subsys_attr_map{"PowerPlant"}->{"release_target"} = "release_tgt_powerplant";
$_pkgsubsys_swdlv_subsys_attr_map{"PowerPlant"}->{"config_id"} = "config_id_powerplant";
$_pkgsubsys_swdlv_subsys_attr_map{"PowerPlant"}->{"resolver"} = "resolver_powerplant";
$_pkgsubsys_swdlv_subsys_attr_map{"PowerPlant"}->{"problem_description"} = "subsys_changes_powerplant";
$_pkgsubsys_swdlv_subsys_attr_map{"Install"}->{"request_type"} = "request_cat_install";
$_pkgsubsys_swdlv_subsys_attr_map{"Install"}->{"release_target"} = "release_tgt_install";
$_pkgsubsys_swdlv_subsys_attr_map{"Install"}->{"resolver"} = "resolver_install";
$_pkgsubsys_swdlv_subsys_attr_map{"Install"}->{"problem_description"} = "subsys_changes_install";
$_pkgsubsys_swdlv_subsys_attr_map{"EMDEC"}->{"request_type"} = "request_cat_emdec";
$_pkgsubsys_swdlv_subsys_attr_map{"EMDEC"}->{"release_target"} = "release_tgt_emdec";
$_pkgsubsys_swdlv_subsys_attr_map{"EMDEC"}->{"config_id"} = "config_id_emdec";
$_pkgsubsys_swdlv_subsys_attr_map{"EMDEC"}->{"resolver"} = "resolver_emdec";
$_pkgsubsys_swdlv_subsys_attr_map{"EMDEC"}->{"problem_description"} = "subsys_changes_emdec";

# Mapping of locomotive models to MPU config id's.
# 	Format:  $_mpu_config_id_for_loco_model{"LOCOMOTIVE_MODEL"} = "MPU_CONFIG_ID";
#	Note:  Only locomotive models that use MPU need to be included in the map.
my %_mpu_config_id_for_loco_model;
$_mpu_config_id_for_loco_model{""} = 					"N/A";
$_mpu_config_id_for_loco_model{"GT38AC"} =				"08_GT38AC";
$_mpu_config_id_for_loco_model{"GT42CU-ACe with MPU"} =	"05_GT42CU-AC";
$_mpu_config_id_for_loco_model{"GT46C-AC"} =			"04_GT46C-AC";
$_mpu_config_id_for_loco_model{"GT46MAC (WDG4)"} =		"06_GT46MAC";
$_mpu_config_id_for_loco_model{"GT46PAC (WDP4B)"} =		"07_GT46MAC_130KPH";
$_mpu_config_id_for_loco_model{"GT50AC (WDG5)"} =		"09_GT50AC";
$_mpu_config_id_for_loco_model{"JT56ACe"} =				"02_JT56ACe";
$_mpu_config_id_for_loco_model{"SD70ACe"} =				"01_SD70ACe";
$_mpu_config_id_for_loco_model{"SD70ACe/LC"} =			"01_SD70ACe";
$_mpu_config_id_for_loco_model{"SD70ACs"} =				"11_SD70ACs";
$_mpu_config_id_for_loco_model{"SD80ACe"} =				"03_SD80ACe";
$_mpu_config_id_for_loco_model{"GT46AC"} =				"10_GT46AC";
$_mpu_config_id_for_loco_model{"EURO3000AC"} =			"12_EURO3000AC";
$_mpu_config_id_for_loco_model{"SD70ACeP4"} =			"13_SD70ACeP4";
$_mpu_config_id_for_loco_model{"GT46ACS"} =			"14_GT46ACS";
$_mpu_config_id_for_loco_model{"JT46AC"} =			"15_JT46AC";
$_mpu_config_id_for_loco_model{"GT52ACe"} =                      "16_GT52ACe";

my %_alloc_hours_per_wecategory_spec;
$_alloc_hours_per_wecategory_spec{"High"} = 140;
$_alloc_hours_per_wecategory_spec{"Medium"} = 70;
$_alloc_hours_per_wecategory_spec{"Low"} = 24;
$_alloc_hours_per_wecategory_spec{"None"} = 0;
$_alloc_hours_per_wecategory_spec{"Unknown"} = $_alloc_hours_per_wecategory_spec{"High"};
$_alloc_hours_per_wecategory_spec{""} = $_alloc_hours_per_wecategory_spec{"High"};

my %_alloc_hours_per_wecategory_sw;
$_alloc_hours_per_wecategory_sw{"High"} = 100;
$_alloc_hours_per_wecategory_sw{"Medium"} = 50;
$_alloc_hours_per_wecategory_sw{"Low"} = 30;
$_alloc_hours_per_wecategory_sw{"None"} = 0;
$_alloc_hours_per_wecategory_sw{"Unknown"} = $_alloc_hours_per_wecategory_sw{"High"};
$_alloc_hours_per_wecategory_sw{""} = $_alloc_hours_per_wecategory_sw{"High"};

#my %_alloc_hours_per_wecategory_trace;
#$_alloc_hours_per_wecategory_trace{"High"} = 100;
#$_alloc_hours_per_wecategory_trace{"Medium"} = 50;
#$_alloc_hours_per_wecategory_trace{"Low"} = 30;
#$_alloc_hours_per_wecategory_trace{"None"} = 0;
#$_alloc_hours_per_wecategory_trace{"Unknown"} = alloc_hours_per_wecategory_trace{"High"};
#$_alloc_hours_per_wecategory_trace{""} = alloc_hours_per_wecategory_trace{"High"};


my $hours_per_workday = 6.4;		# Standard effective hours in an 8-hour day
my $offset_from_spec_due_date = 25;     # The offset of days before High SW work effort spec due date

my %_uses_lc_specific_cr_release;
	# 0 if product/system uses the same target release for parent,
	#	spec, sw, and db CRs
	# 1 if product/system uses different target releases based on
	#	the CR lifecycle
$_uses_lc_specific_cr_release{""}{""} 				= 0;
$_uses_lc_specific_cr_release{"EM2000"}{""} 		= 1;
$_uses_lc_specific_cr_release{"EM2000"}{"RailCAN"} 	= 0;
$_uses_lc_specific_cr_release{"Power Plant"}{""}	= 1;

my %_uses_spec_sw_component_release;
	# 0 if component_name is for a product/system that does not use "_Spec"
	#	and "_SW" component names in release tags
	# 1 if component_name is for a product/system that uses "_Spec" and "_SW"
	#	component names in release tags
$_uses_spec_sw_component_release{""} 				= 0;
$_uses_spec_sw_component_release{"EM2K"} 			= 1;
$_uses_spec_sw_component_release{"PowerPlant"}	 	= 1;

# End of variables that have parallel definitions in the shared_tables.js file

sub get_cr_copyrelationship {

	my $key=shift;
	return $_cr_copyrelationships{$key};
}

sub get_childcr_prependtext {

	my $key=shift;
	return $_childcr_prependtext{$key};
}

sub get_lifecycletag {

	my $key=shift;
	return $_lifecycletag{$key};
}

sub get_efficiency_rlsbld {
	return $_efficiency_rlsbld;
}

sub get_efficiency_swdev {
	my $key=shift;
	if (defined($_efficiency_swdev{$key}))
	{
		return $_efficiency_swdev{$key};
	}
	else { return $_efficiency_swdev{""}; }
}

sub get_efficiency_specdev {
	return $_efficiency_specdev;
}

sub get_efficiency_tracdev {
        return $_efficiency_tracdev;
}

sub get_dwelltime_spec2sw {
	my $key=shift;
	if (defined($_dwelltime_spec2sw{$key}))
	{
		return $_dwelltime_spec2sw{$key};
	}
	else { return $_dwelltime_spec2sw{""}; }
}

sub get_product_name {
	my $key=shift;
	if (defined($_product_name{$key}))
	{
		return $_product_name{$key};
	}
	else { return $_product_name{""}; }
}

sub get_release_stream_level {
	my $key=shift;
	if (defined($_release_stream_level{$key}))
	{
		return $_release_stream_level{$key};
	}
	else { return $_release_stream_level{""}; }
}

sub get_uses_release_crs {
	my $key=shift;
	if (defined($_uses_release_crs{$key}))
	{
		return $_uses_release_crs{$key};
	}
	else { return $_uses_release_crs{""}; }
}

sub get_alloc_hours_per_wecategory_spec {
	my $key=shift;
	if (defined($_alloc_hours_per_wecategory_spec{$key}))
	{
		return $_alloc_hours_per_wecategory_spec{$key};
	}
	else { return $_alloc_hours_per_wecategory_spec{""} };
}

sub get_alloc_hours_per_wecategory_sw {
	my $key=shift;
	if (defined($_alloc_hours_per_wecategory_sw{$key}))
	{
		return $_alloc_hours_per_wecategory_sw{$key};
	}
	else { return $_alloc_hours_per_wecategory_sw{""}; }
}

#sub get_alloc_hours_per_wecategory_trace {
#        my $key=shift;
#	if (defined($_alloc_hours_per_wecategory_trace{$key}))
#	{
#	        return $_alloc_hours_per_wecategory_trace{$key};
#	}
#	else { return $_alloc_hours_per_wecategory_trace{""} };
#}

sub get_hours_per_workday {

	return $hours_per_workday;
}

sub get_pkg_duration_ft_rel() {
	return $_pkg_duration_ft_rel;
}

sub get_pkg_duration_ft_rel_ext() {
	return $_pkg_duration_ft_rel_ext;
}

sub get_pkg_duration_subsys() {
	my @durations = 
		sort { $_pkg_duration_subsys{$b} <=> $_pkg_duration_subsys{$a} } keys %_pkg_duration_subsys;

	return $_pkg_duration_subsys{$durations[0]};
}

sub get_pkg_duration_subsys_ext() {
	my @durations = 
		sort { $_pkg_duration_subsys_ext{$b} <=> $_pkg_duration_subsys_ext{$a} } keys %_pkg_duration_subsys_ext;

	return $_pkg_duration_subsys_ext{$durations[0]};
}

sub get_pkg_duration_can() {
	return $_pkg_duration_can;
}

sub get_pkg_duration_char() {
	return $_pkg_duration_char;
}

#input component name
sub get_process_name{
	my $key=shift;
	if (defined($_process_name{$key}))
	{
		return $_process_name{$key};
	}
	else { return $_process_name{""}; }
}

# used in release.pl
sub ref_product_to_release_tgt_query_str{
	return \%_product_to_release_tgt_query_str;
}

#returns the companion os for the specified os
sub get_companion_os_name
{
	my $key=shift;
	if (defined($_companion_os_name{$key}))
	{
		return $_companion_os_name{$key};
	}
	else { return $_companion_os_name{""}; }
}

	
# The Change GUI uses the .js parallel to the following subroutines, which have the same names,
#	and are defined in the shared_tables.js file in the package template.  Any changes to
#	the logic and/or values here need to be reflected in the shared_tables.js file as well.
#

sub get_uses_lc_specific_cr_release {
	my $product_key=$_[0];
	my $system_key=$_[1];	
	if (defined($_uses_lc_specific_cr_release{$product_key}))
	{
		if (defined($_uses_lc_specific_cr_release{$product_key}{$system_key}))
		{
			return $_uses_lc_specific_cr_release{$product_key}{$system_key};
		}
		else { return $_uses_lc_specific_cr_release{$product_key}{""}; }
	}
	else { return $_uses_lc_specific_cr_release{""}{""}; }
}

sub get_uses_spec_sw_component_release {
	my $key=shift;
	if (defined($_uses_spec_sw_component_release{$key}))
	{
		return $_uses_spec_sw_component_release{$key};
	}
	else { return $_uses_spec_sw_component_release{""}; }
}



# Subroutines for getting subsystem info attribute names and mappings

#returns array of subsystem names
sub get_pkg_subsystems{
	return(keys(%_pkgsubsys_swdlv_subsys_attr_map));
}

#returns value of config_id_mpu attribute for specified locomotive model
sub get_mpu_config_id_for_loco_model{
	my $key=shift;
	if (defined($_mpu_config_id_for_loco_model{$key}))
	{
		return($_mpu_config_id_for_loco_model{$key});
	}
	else { return $_mpu_config_id_for_loco_model{""}; }
}

# End of subroutines that have parallel definitions in the shared_tables.js file



#input subsystem name, as used in pkg_subsystem_name attribute on PkgSubsystem CRs
#returns mapping of subsystem info for the specified subsytem - 
#	reference to hash of Pkg Subsystem CR attribute names (as keys) and SW Deliverable CR 
#	attribute names (as values)
sub get_subsys_attr_map_pkgsubsys2swdlv{
	my $key=shift;
	if (defined($_pkgsubsys_swdlv_subsys_attr_map{$key}))
	{
		return $_pkgsubsys_swdlv_subsys_attr_map{$key};
	}
	else { return {}; }
}

#input subsystem name, as used in pkg_subsystem_name attribute on PkgSubsystem CRs
#returns mapping of subsystem info for the specified subsytem - 
#	reference to hash of SW Deliverable CR attribute names (as keys) and Pkg Subsystem CR 
#	attribute names (as values)
sub get_subsys_attr_map_swdlv2pkgsubsys{
	my $key=shift;
	my %_swdlv2pkgsubsys_attr_map;
	
	if (defined($_pkgsubsys_swdlv_subsys_attr_map{$key}))
	{
		%_swdlv2pkgsubsys_attr_map = reverse %{$_pkgsubsys_swdlv_subsys_attr_map{$key}};
		return(\%_swdlv2pkgsubsys_attr_map);
	}
	else{ return {} ; }
}

#input subsystem name, as used in pkg_subsystem_name attribute on PkgSubsystem CRs,
#	and name of attribute on Pkg Subsystem CR.
#returns mapping of subsystem info for the specified subsytem and Pkg Subsystem CR attribute name - 
#	reference to hash of SW Deliverable CR attribute name (as key) and Pkg Subsystem CR 
#	attribute name (as value)
sub get_single_subsys_attr_map_pkgsubsys2swdlv{
	my $key=shift;
	my $attr_name=shift;
	my %_attr_map;
	
	if (defined($_pkgsubsys_swdlv_subsys_attr_map{$key}))
	{
		if (defined($_pkgsubsys_swdlv_subsys_attr_map{$key}->{$attr_name}))
		{
			$_attr_map{$_pkgsubsys_swdlv_subsys_attr_map{$key}->{$attr_name}} = $attr_name;
			return(\%_attr_map);
		}
		else{ return {}; }
	}
	else{ return {}; }
}

#input subsystem name, as used in pkg_subsystem_name attribute on PkgSubsystem CRs,
#	and name of attribute on SW Deliverable CR.
#returns mapping of subsystem info for the specified subsytem and SW Deliverable attribute name - 
#	reference to hash of PkgSubsystem CR attribute name (as key) and SW Deliverable CR 
#	attribute name (as value)
sub get_single_subsys_attr_map_swdlv2pkgsubsys{
	my $key=shift;
	my $attr_name=shift;
	my %_swdlv2pkgsubsys_attr_map;
	my %_attr_map;
	
	if (defined($_pkgsubsys_swdlv_subsys_attr_map{$key}))
	{
		%_swdlv2pkgsubsys_attr_map = reverse %{$_pkgsubsys_swdlv_subsys_attr_map{$key}};
		if (defined($_swdlv2pkgsubsys_attr_map{$attr_name}))
		{
			$_attr_map{$_swdlv2pkgsubsys_attr_map{$attr_name}} = $attr_name;
			return(\%_attr_map);
		}
		else{ return {}; }
	}
	else{ return {}; }
}

#returns array of request_cat_[subsystem] attribute names
sub get_subsys_request_cat_attrs{
	my @_attrs_list;
	
	foreach my $subsystem (keys(%_pkgsubsys_swdlv_subsys_attr_map))
	{
		foreach my $subsys_attr (keys(%{$_pkgsubsys_swdlv_subsys_attr_map{$subsystem}}))
		{
			if ('request_type' eq $subsys_attr){
				push(@_attrs_list,$_pkgsubsys_swdlv_subsys_attr_map{$subsystem}->{$subsys_attr});
			}
		}
	}
	return(@_attrs_list);
}

#returns array of release_tgt_[subsystem] attribute names
sub get_subsys_release_tgt_attrs{
	my @_attrs_list;
	
	foreach my $subsystem (keys(%_pkgsubsys_swdlv_subsys_attr_map))
	{
		foreach my $subsys_attr (keys(%{$_pkgsubsys_swdlv_subsys_attr_map{$subsystem}}))
		{
			if ('release_target' eq $subsys_attr){
				push(@_attrs_list,$_pkgsubsys_swdlv_subsys_attr_map{$subsystem}->{$subsys_attr});
			}
		}
	}
	return(@_attrs_list);
}

#returns array of config_id_[subsystem] attribute names
sub get_subsys_config_id_attrs{
	my @_attrs_list;
	
	foreach my $subsystem (keys(%_pkgsubsys_swdlv_subsys_attr_map))
	{
		foreach my $subsys_attr (keys(%{$_pkgsubsys_swdlv_subsys_attr_map{$subsystem}}))
		{
			if ('config_id' eq $subsys_attr){
				push(@_attrs_list,$_pkgsubsys_swdlv_subsys_attr_map{$subsystem}->{$subsys_attr});
			}
		}
	}
	return(@_attrs_list);
}

#returns array of resolver_[subsystem] attribute names
sub get_subsys_resolver_attrs{
	my @_attrs_list;
	
	foreach my $subsystem (keys(%_pkgsubsys_swdlv_subsys_attr_map))
	{
		foreach my $subsys_attr (keys(%{$_pkgsubsys_swdlv_subsys_attr_map{$subsystem}}))
		{
			if ('resolver' eq $subsys_attr){
				push(@_attrs_list,$_pkgsubsys_swdlv_subsys_attr_map{$subsystem}->{$subsys_attr});
			}
		}
	}
	return(@_attrs_list);
}

#returns array of subsys_changes_[subsystem] attribute names
sub get_subsys_description_attrs{
	my @_attrs_list;
	
	foreach my $subsystem (keys(%_pkgsubsys_swdlv_subsys_attr_map))
	{
		foreach my $subsys_attr (keys(%{$_pkgsubsys_swdlv_subsys_attr_map{$subsystem}}))
		{
			if ('problem_description' eq $subsys_attr){
				push(@_attrs_list,$_pkgsubsys_swdlv_subsys_attr_map{$subsystem}->{$subsys_attr});
			}
		}
	}
	return(@_attrs_list);
}

#returns request_cat_[subsystem] attribute name for specified subsystem
sub get_request_cat_attr_for_subsys{
	my $key=shift;
	my $attr_name = "";
	if (defined($_pkgsubsys_swdlv_subsys_attr_map{$key}))
	{
		foreach my $subsys_attr (keys(%{$_pkgsubsys_swdlv_subsys_attr_map{$key}}))
		{
			if ('request_type' eq $subsys_attr){
				return($_pkgsubsys_swdlv_subsys_attr_map{$key}->{$subsys_attr});
			}
		}
	}
	return($attr_name);
}

#returns release_tgt_[subsystem] attribute name for specified subsystem
sub get_release_tgt_attr_for_subsys{
	my $key=shift;
	my $attr_name = "";
	if (defined($_pkgsubsys_swdlv_subsys_attr_map{$key}))
	{
		foreach my $subsys_attr (keys(%{$_pkgsubsys_swdlv_subsys_attr_map{$key}}))
		{
			if ('release_target' eq $subsys_attr){
				return($_pkgsubsys_swdlv_subsys_attr_map{$key}->{$subsys_attr});
			}
		}
	}
	return($attr_name);
}

#returns config_id_[subsystem] attribute name for specified subsystem
sub get_config_id_attr_for_subsys{
	my $key=shift;
	my $attr_name = "";
	if (defined($_pkgsubsys_swdlv_subsys_attr_map{$key}))
	{
		foreach my $subsys_attr (keys(%{$_pkgsubsys_swdlv_subsys_attr_map{$key}}))
		{
			if ('config_id' eq $subsys_attr){
				return($_pkgsubsys_swdlv_subsys_attr_map{$key}->{$subsys_attr});
			}
		}
	}
	return($attr_name);
}

#returns resolver_[subsystem] attribute name for specified subsystem
sub get_resolver_attr_for_subsys{
	my $key=shift;
	my $attr_name = "";
	if (defined($_pkgsubsys_swdlv_subsys_attr_map{$key}))
	{
		foreach my $subsys_attr (keys(%{$_pkgsubsys_swdlv_subsys_attr_map{$key}}))
		{
			if ('resolver' eq $subsys_attr){
				return($_pkgsubsys_swdlv_subsys_attr_map{$key}->{$subsys_attr});
			}
		}
	}
	return($attr_name);
}

#returns subsys_changes_[subsystem] attribute name for specified subsystem
sub get_description_attr_for_subsys{
	my $key=shift;
	my $attr_name = "";
	if (defined($_pkgsubsys_swdlv_subsys_attr_map{$key}))
	{
		foreach my $subsys_attr (keys(%{$_pkgsubsys_swdlv_subsys_attr_map{$key}}))
		{
			if ('problem_description' eq $subsys_attr){
				return($_pkgsubsys_swdlv_subsys_attr_map{$key}->{$subsys_attr});
			}
		}
	}
	return($attr_name);
}

sub get_offset_from_spec_due_date{

        return $offset_from_spec_due_date;
}

# End of subroutines for getting subsystem info attribute names and mappings
1;

